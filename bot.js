const TOKEN = "MTExMTIxOTQ4OTc5OTk0MjIxNA.GDNSBd._bEpFFSgnG5OIzUkQuSZ4G8M-QsaJv0q8gT_e0";
const CLIENT_ID = "1111219489799942214";
const URL = "https://discordapp.com/oauth2/authorize?&client_id=1111219489799942214&scope=bot&permissions=274878220288";

const { REST, Routes, PermissionsBitField } = require('discord.js');
var fs = require('fs');

const { Client, GatewayIntentBits } = require('discord.js');
const { randomBytes } = require('crypto');
const client = new Client({ intents: [GatewayIntentBits.Guilds] });

var data = {};

var admin = "124136556578603009";

const commands = [
    {
        name: 'ping',
        description: 'Zegt pong!',
        execute: function (interaction)
        {
            interaction.reply('Pong!');
        }
    },
    {
        name: 'opdatum',
        description: 'Voer dit commando uit om de bot op te datumen!',
        admin: true,
        execute: function (interaction)
        {
            interaction.reply('OK Doei!').then(repl =>
            {
                setTimeout(() =>
                {
                    please.crash.the.bot.now();
                }, 1000);
            });
        }
    },
    {
        name: 'maakadmin',
        description: 'Maak iemand adminder!',
        admin: true,
        options: [{ type: 6, name: "gebruiker", description: "Wiemst", required: true }],
        execute: function (interaction)
        {
            data.servers[interaction.guildId].admins.push(interaction.options.data[0].value);
            interaction.reply("<@" + interaction.options.data[0].value + "> is nu adminder van Cirkelscore in deze ober!");
        }
    },
    {
        name: 'verwijderadmin',
        description: 'Maak iemand niet meer adminder!',
        admin: true,
        options: [{ type: 6, name: "gebruiker", description: "Wiemst", required: true }],
        execute: function (interaction)
        {
            if ("124136556578603009" == interaction.options.data[0].value)
            {
                interaction.reply({ content: "Mag niet!", ephemeral: true });
                return;
            }

            let index = data.servers[interaction.guildId].admins.indexOf(interaction.options.data[0].value);
            if (index >= 0)
            {
                data.servers[interaction.guildId].admins.splice(index);
                interaction.reply("<@" + interaction.options.data[0].value + "> is nu niet meer adminder van Cirkelscore in deze ober!");
            }
            else
            {
                interaction.reply({ content: "<@" + interaction.options.data[0].value + "> is helemaal geen adminder!", ephemeral: true });
            }
        }
    },
    {
        name: 'maaktiem',
        description: 'Maak een nieuw tiem!',
        admin: true,
        options: [{ type: 3, name: "naam", description: "Geef tiem een naam", required: true }, { type: 3, name: "alias", description: "korte alias voor commando's", required: true }],
        execute: function (interaction)
        {
            let alias = interaction.options.getString("alias").trim().toUpperCase();

            if (!data.servers[interaction.guildId].teams[alias])
            {
                data.servers[interaction.guildId].teams[alias] =
                {
                    name: interaction.options.getString("naam").trim(),
                    score: 0,
                    members: []
                };

                interaction.reply("Tiem '" + interaction.options.getString("naam").trim() + "' gemaakt!");
            }
            else
            {
                interaction.reply("De alias '" + alias + "' is al in gebruik door tiem '" + data.servers[interaction.guildId].teams[alias].name + "'!");
            }
        }
    },
    {
        name: 'verwijdertiem',
        description: 'Vernietig een tiem permanent!',
        admin: true,
        options: [{ type: 3, name: "alias", description: "Welke moet weg? (alias)", required: true }],
        execute: function (interaction)
        {
            let alias = interaction.options.getString("alias").trim().toUpperCase();
            if (data.servers[interaction.guildId].teams[alias])
            {
                delete (data.servers[interaction.guildId].teams[alias]);

                interaction.reply("Tiem '" + alias + "' vernietigd!");
            }
            else
            {
                interaction.reply("De alias '" + alias + "' bestaat niet!");
            }
        }
    },
    {
        name: 'scorebord',
        description: 'Genereer scorebord!',
        options: [],
        execute: function (interaction)
        {
            let tiems = [];

            for (const [key, value] of Object.entries(data.servers[interaction.guildId].teams)) 
            {
                tiems.push(value);
                value.alias = key;
            }

            if (tiems.length == 0)
            {
                interaction.reply("Er zijn nog geen tiems!");
                return;
            }

            tiems.sort(function (a, b) { return b.score - a.score; });
            let str = "";

            let first = 0;
            let second = 0;
            let third = 0;

            for (let i = 0; i < tiems.length; i++)
            {
                let score = tiems[i].score;

                if (score > first)
                {
                    third = second;
                    second = first;
                    first = score;
                }
                else if (score > second && score != first)
                {
                    third = second;
                    second = score;
                }
                else if (score > third && score != first && score != second)
                {
                    third = score;
                }
            }

            console.log("score tiers: ", first, second, third);

            for (let i = 0; i < tiems.length; i++)
            {
                let medal = "  ";
                if (tiems[i].score == first) medal = "🥇 ";
                else if (tiems[i].score == second) medal = "🥈 ";
                else if (tiems[i].score == third) medal = "🥉 ";

                str += "> ## **" + medal + tiems[i].name + " [" + tiems[i].alias + "]**:\n> 		" + scoreName(tiems[i].score, interaction) + "\n";
            }

            console.log(tiems);

            interaction.reply(str);
        }
    },
    {
        name: 'geefpuntjes',
        description: 'Geef een tiem puntjes!',
        admin: true,
        options: [{ type: 3, name: "alias", description: "Welke krijgt puntjes?", required: true }, { type: 10, name: "puntjes", description: "hoeveel puntjes?", required: true }],
        execute: function (interaction)
        {
            let alias = interaction.options.getString("alias").trim().toUpperCase();
            let points = interaction.options.getNumber("puntjes");

            if (data.servers[interaction.guildId].teams[alias])
            {
                data.servers[interaction.guildId].teams[alias].score += points;

                interaction.reply("**" + data.servers[interaction.guildId].teams[alias].name + "** heeft " + scoreName(points, interaction) + " gekregen!");
            }
            else
            {
                interaction.reply("De alias '" + alias + "' bestaat niet!");
            }
        }
    },
    {
        name: 'neempuntjes',
        description: 'Neem puntjes van een tiem!',
        admin: true,
        options: [{ type: 3, name: "alias", description: "Welke verliest puntjes?", required: true }, { type: 10, name: "puntjes", description: "hoeveel puntjes?", required: true }],
        execute: function (interaction)
        {
            let alias = interaction.options.getString("alias").trim().toUpperCase();
            let points = interaction.options.getNumber("puntjes");

            if (data.servers[interaction.guildId].teams[alias])
            {
                data.servers[interaction.guildId].teams[alias].score -= points;

                interaction.reply("**" + data.servers[interaction.guildId].teams[alias].name + "** heeft " + scoreName(points, interaction) + " verloren!");
            }
            else
            {
                interaction.reply("De alias '" + alias + "' bestaat niet!");
            }
        }
    },
    {
        name: 'zetpuntjes',
        description: 'Zet de score van een tiem op een exact getal!',
        admin: true,
        options: [{ type: 3, name: "alias", description: "Welke krijgt puntjes?", required: true }, { type: 10, name: "puntjes", description: "hoeveel puntjes?", required: true }],
        execute: function (interaction)
        {
            let alias = interaction.options.getString("alias").trim().toUpperCase();
            let points = interaction.options.getNumber("puntjes");

            if (data.servers[interaction.guildId].teams[alias])
            {
                data.servers[interaction.guildId].teams[alias].score = points;

                interaction.reply("**" + data.servers[interaction.guildId].teams[alias].name + "** heeft nu " + scoreName(points, interaction) + "!");
            }
            else
            {
                interaction.reply("De alias '" + alias + "' bestaat niet!");
            }
        }
    },
    {
        name: 'puntnaam',
        description: 'Verander hoe puntjes heten!',
        admin: true,
        options: [{ type: 3, name: "enkelvoud", description: "1 punt", required: true }, { type: 3, name: "meervoud", description: "5 punten", required: true }],
        execute: function (interaction)
        {
            data.servers[interaction.guildId].scorenameSingle = interaction.options.getString("enkelvoud").trim();
            data.servers[interaction.guildId].scorenameMultiple = interaction.options.getString("meervoud").trim();
            interaction.reply("Punten zijn nu '" + data.servers[interaction.guildId].scorenameSingle + "' of '" + data.servers[interaction.guildId].scorenameMultiple + "'!");
        }
    },
];

const rest = new REST({ version: '10' }).setToken(TOKEN);

(async () =>
{
    try
    {
        console.log('Started refreshing application (/) commands.');

        await rest.put(Routes.applicationCommands(CLIENT_ID), { body: commands });

        console.log('Successfully reloaded application (/) commands.');
    }
    catch (error)
    {
        console.error(error);
    }
})();

client.on('ready', () =>
{
    console.log(`Logged in as ${client.user.tag}!`);

    loadData(function (newData)
    {
        data = newData;
        console.log("DATA LOADED", data);
    });
});

function loadData (complete)
{
    fs.readFile('./botData.json', function read (err, data) 
    {
        if (err) 
        {
            data = "{}";
            console.log("no data found");
        }
        try
        {
            data = JSON.parse(data);
            console.log("savedata loaded:", data);
        }
        catch (e)
        {
            if (data == null)
            {
                console.log("Memory broken, will attempt to restore backup.");
            }
        }

        if (data == null || data == {})
        {
            fs.readFile('./botDataBackup.json', function read (err, data)
            {
                if (err)
                {
                    data = "{}";

                    console.log("no data found in backup either");
                }
                try
                {
                    data = JSON.parse(data);
                    console.log("Backup restored");
                }
                catch (e)
                {
                    if (data == null)
                    {
                        data = {};
                        console.log("Memory backup broken, making new");
                        saveBackup = false;
                    }
                }
                initialized = true;
            });
        }
        else //successfully loaded
        {
            client.saveBackup(true);

            initialized = true;
        }

        data = data || {};

        data.servers = data.servers || {};

        complete(data);
    });
}

function scoreName (points, interaction)
{
    return points + " " + (points == 1 ? data.servers[interaction.guildId].scorenameSingle : data.servers[interaction.guildId].scorenameMultiple);
}

client.on('interactionCreate', function (interaction)
{
    if (!interaction.isChatInputCommand()) return;

    data.servers = data.servers || {};
    data.servers[interaction.guildId] = data.servers[interaction.guildId] || {};
    data.servers[interaction.guildId].admins = data.servers[interaction.guildId].admins || [];
    data.servers[interaction.guildId].teams = data.servers[interaction.guildId].teams || {};

    data.servers[interaction.guildId].scorenameSingle = data.servers[interaction.guildId].scorenameSingle || "punt";
    data.servers[interaction.guildId].scorenameMultiple = data.servers[interaction.guildId].scorenameMultiple || "punten";

    for (let i = 0; i < commands.length; i++)
    {
        if (commands[i].name === interaction.commandName)
        {
            if (commands[i].admin)
            {
                let admins = data.servers[interaction.guildId].admins || [];

                console.log("adminder check:" + admins.indexOf(interaction.user.id), admins);

                let isServerAdmin = interaction.member.permissions.has(PermissionsBitField.Flags.Administrator);
                let isBotAdmin = admins.indexOf(interaction.user.id) >= 0;
                let isHardcodedAdmin = interaction.user.id == admin;
                let isOwner = interaction.member.guild.ownerId == interaction.user.id;

                let forbidden = (!isBotAdmin && !isHardcodedAdmin && !isServerAdmin && !isOwner);

                console.log(forbidden);

                if (forbidden)
                {
                    console.log("blocked");
                    interaction.reply({ content: "Mag niet!", ephemeral: true });
                    return;
                }
            }

            console.log("Current Data:", data);
            commands[i].execute(interaction);
            saveData(data);
            return;
        }
    }
});

client.saveBackup = function (silent = false)
{
    console.log("Writing Backup");
    fs.writeFile("./botDataBackup.json", JSON.stringify(data), function (err)
    {
        if (err)
        {
            console.log(err);
        }
    });
};

saveData = function (newData, callback, context)
{
    newData = newData || {};
    let json = JSON.stringify(newData);
    console.log("Saving...", newData, json);
    try
    {
        fs.writeFile("./botData.json", json, function (err)
        {
            if (err)
            {
                console.log(err);
            }

            console.log("Saved.", json);

            context = context || this;
            callback && callback.call(context, true);
        });
    }
    catch (ex)
    {
        console.log("Error while saving!");
        console.log(ex);
        context = context || this;
        callback && callback.call(context, false);
    }
};

client.login(TOKEN);